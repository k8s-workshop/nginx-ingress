Nginx Ingress
=====

Vamos agora trabalhar com o Ingress!

Antes de começarmos, vamos alterar nosso cluster para possibilitar o uso das portas 80 e 443:

- SSH no  Kubernetes master
- Abra /etc/kubernetes/manifests/kube-apiserver.yaml para editar (vi, vim, nano, etc...)
- Dentro do arquivo, na seção "command" adicione: - --service-node-port-range=80-32767
- Salve e feche o arquivo
- Reinicie Kubernetes master.


Para esta seção, vamos criar o namespace "nginx-ingress", com o seguinte comando:

```
# kubectl create ns nginx-ingress
```

Resultado: 
`namespace/nginx-ingress created`

Executar na seguinte ordem:


# 1 - Criar o projeto Java que usaremos como exemplo:

```
# kubectl apply -f cars-api-deployment.yaml -n nginx-ingress
# kubectl apply -f cars-api-service.yaml -n nginx-ingress
```

Resultado: 

`deployment.apps/cars-api created`
`service/cars-api created`


# 2 - Criar o backend default:

- Responde uma página com código 404 no caminho /
- Responde uma página com código 200 no caminho /healthz

```
# kubectl apply -f default-http-backend.yaml -n nginx-ingress
# kubectl apply -f default-http-backend-svc.yaml -n nginx-ingress
```

Resultado: 

`deployment.extensions/default-http-backend created`
`service/default-http-backend created`


Vamos chegar o que fizemos:

```
#kubectl get all -n nginx-ingress
```

![Defautl Backend](imagens/default-backend.png)


# 3 - Criação de certificado:

Primeiro, crie a seguinte pasta:

```
# mkdir /etc/nginx-ssl/dhparam
```

Entre na pasta:

```
# cd /etc/nginx-ssl/dhparam
```

Execute o seguinte comando para a chave e o certificado :

```
# openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tls-key.key -out tls-cert.crt
```

Informe os valores solicitados. Sugestão:

- Country Name (2 letter code) [AU]: BR
- State or Province Name (full name) [Some-State] : PARÁ
- Locality Name (eg, city) []:BELÉM
- Organization Name (eg, company) [Internet Widgits Pty Ltd]:VIBE DESENVOLVIMENTO
- Organizational Unit Name (eg, section) []:DEVOPS
- Common Name (e.g. server FQDN or YOUR name) []:meudominio.com
- Email Address []:devops@vibedesenv.com

Coverta para o formato PEM:

```
# openssl dhparam -out dhparam.pem 2048
```

Será exebida a seguinte mensagem:

Generating DH parameters, 2048 bit long safe prime, generator 2
This is going to take a long time .............................

Após a finalização,  liste o diretório com o comando "ls" e veja três arquivos:

```
# dhparam.pem  tls-cert.crt  tls-key.key
```

# 4 - Criação dos Secrets:

Continue no diretório /etc/nginx-ssl/dhparam

```
# kubectl create secret tls tls-certificate --key tls-key.key --cert tls-cert.crt --namespace nginx-ingress
# kubectl create secret generic tls-dhparam --from-file=dhparam.pem --namespace nginx-ingress
```

Resultado: 

`secret/tls-certificate created`
`secret/tls-dhparam created`


# 5 - Criação das roles pro Nginx:

```
# kubectl apply -f nginx-roles.yml -n nginx-ingress
```

Resultado: 

`clusterrole.rbac.authorization.k8s.io/nginx-role created`
`clusterrolebinding.rbac.authorization.k8s.io/nginx-role created`


# 6 - Criação do Nginx Ingress Controller:

```
# kubectl apply -f nginx-controller.yaml -n nginx-ingress
# kubectl apply -f nginx-ingress-svc.yaml -n nginx-ingress
```

Resultado: 

`deployment.extensions/nginx-ingress-controller created`
`service/nginx-ingress created`


Vamos chegar o que fizemos:

```
#kubectl get all -n nginx-ingress
```

![Defautl Backend](imagens/nginx-ingress-controller.png)


# 7 - Adicionando IP Externo para o nginx-ingress service:

Entre no seu cloud provider e obtenha o IP externo.

Ex: 35.229.55.68

Edite o serviço:

```
# kubectl edit service nginx-ingress -n nginx-ingress
```

Adicione abaixo de type: LoadBalancer:

```
externalIPs:
- 35.229.55.68
```

O arquivo fica similar ao abaixo:

![Defautl Backend](imagens/external-ip.png)

Salve o arquivo.

Você receberá a seguinte mensagem:

`service/nginx-ingress edited



Resultado: 

`deployment.extensions/nginx-ingress-controller created`
`service/nginx-ingress created`


Vamos chegar o que fizemos:

```
#kubectl get all -n nginx-ingress
```

![Defautl Backend](imagens/external-ip2.png)

# 8 - Acessando nosso backend default:

curl ou browser (obtenha o IP externo)

Exemplo:

```
# curl -v http://35.229.55.68:32176/
# curl -v http://35.229.55.68:32176/healthz
```

# 9 - Criando o nosso Ingress Resource:

```
# kubectl create -f ingress-all-address-cars-api.yaml -n nginx-ingress
```

Resultado: 

`ingress.extensions/cars-ingress created`


Observação:

O arquivo cars-ingress.yaml poderia ser utilizado, caso tivéssemos um domínio válido.


Verificando o recurso criado:

```
#kubectl get ingress -n  nginx-ingress
```

Resultado:

```
NAME           HOSTS              ADDRESS   PORTS     AGE
cars-ingress   *                            80, 443   102s
```


# 10 - Alterando as portas do serviço :

```
# kubectl edit service nginx-ingress -n nginx-ingress
```

Existem duas tags "nodePort" no arquivo. Coloque o valor 80 na primeira e 443 para a segunda.
Salve e feche o arquivo.

Resultado:

`service/nginx-ingress edited`

Arquivo deve ficar similar ao abaixo:

![Defautl Backend](imagens/alteracao-porta.png)


# 11 - Acessando a aplicação cars-api através do Ingress :

Obs: Coloque seu IP Externo

```
# curl -v http://IP_EXTERNO/cars/list
# curl -v https://IP_EXTERNO/cars/list --insecure
```

Ou aacesse pelo seu browser!

